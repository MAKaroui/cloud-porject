LATEST_BACKUP=$(velero get backups | sed '$!d' | cut -d " " -f1)


velero restore create --from-backup "$LATEST_BACKUP"


RESTORED_PV=$(kubectl get pv | sed '$!d' | cut -d " " -f1)

kubectl apply -f - <<EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mo-data-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage:  # Add your desired storage size here
  dataSource:
    name: $RESTORED_PV
    kind: PersistentVolume
EOF

# Update application deployment to use the restored PVC
kubectl patch deployment your-deployment \
  --patch '{"spec":{"template":{"spec":{"volumes":[{"name":"mo-data","persistentVolumeClaim":{"claimName":"'"mo-data-pvc"'"}}]}}}}'

kubectl apply -f mongo-conf.yml 
kubectl apply -f mongo-depl.yml 
kubectl apply -f app-depl.yml 


echo "Disaster recovery completed!"
